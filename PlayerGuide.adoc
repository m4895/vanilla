= Vanilla Player Guide
BigBear
v1.0, 2022-02
:toc: left

== Introduction
Vanilla is a type of game for minetest. This document is a guide targeted to
players not developers or people who want to develop, host and customise their
own server.  Unless explictily stated it assumes the default settings with no
additional mods.  It also assumes you are playing the game in survival mode
unless stated otherwise.

== Quick Start Guide

=== Starting the Game and basic controls
If you have never played minetest before - please see
https://wiki.minetest.net/Getting_Started[Minetest Getting Started]

To play locally you can download the game from contentdb using the 'Content'
tab in minetest.  Alternatively you could just join a server that runs it in
'Join Game' tab.  or you can connect to a server that is based on Vanilla.
footnote:[The Bear Lands (mt.ulsterit.com:50000) is a Vanilla server hosted by the author.]


=== Day 1 - Don't get killed!
There is no right or wrong way to the play the game, but below is a general
guide as to what you probably want to be doing in the first day or two.

==== Avoid Monsters
Monsters will generally spawn in dark places. For now, you should avoid
monsters at all costs.  Don't stray too deep into dark caverns, keep an eye on
the position of the sun so you arn't caught out by nightfall. You won't have
good weapons or good armour yet, so you should probably just run away from any
monsters you might encounter. 

TIP: The 'sprint' key (e) will help you run away.

==== Make Basic tools
The first task is to make some tools. For this you need wood, find a tree and
punch it until you get some. This is very slow, so as soon as you get a few
blocks you should make an axe. put wood into your crafting grid, convert to
planks, then make a wooden axe and a wooden pick. Chop two or three trees down
with your axe.

==== Gather Food
Avoid sprinting! only sprint if you are in danger, otherwise your food will run
out too quickly. If you can find apples or blueberries, great, they are ideal
for avoiding starvation in the first day or two. Don't worry about farming yet
unless there is no easier option.

==== Mine Stone and Coal
Coal can be very handy, examine cliffs and cave entrances to find some. When
you find a good spot first dig out some stone and make a stone set of tools
(pick, sword, axe, spade).  Then use your new pick to dig out any coal you have
found. 

==== Make Shelter
If you have got all the above done before your first nightfall you are doing
really well.  Now you need a safe place to hole up for the night. Nothing
fancy, you can dig into a cliff if there are any handy, block off a cave
entrance, or dig into a cliff face. Nothing fancy you just want to create a
'survival bunker' - a small safe place that is completely sealed.  You will can
use the coal to make torches to light it up on the inside. 


==== Work in shelter for first night
Once you in a safe place to wait for the next day you can start making it a
little more comfortable. Create a sign, then craft that sign into a crafting
guide sign and put it on the wall. Use this to figure out how to craft other
items.  Craft some wood fence posts, and knock out a few blocks in the shelter
and fill them with fence posts to make crude windows.

As you wait for morning you should also create a full set of wooden armour and
a shield and equip it.  While you wait for morning Dig a staircase downwards to
collect more cobble, (and coal if you can find it).

When sun comes up, go out, collect more wood, and food. You should now be in a
good position to start building, exploring and mining.

== Game Elements

=== Tools
Tools are fundamental to gameplay, use the right tool for the right task

.Main Tools
* *hand* - Some items may be dug without a tool, but is often slow/ineffective.
* *pick* - Used for mining stone and minerals
* *axe* - Used for chopping wood and wooden items may also be used as a weapon
* *shovel* - Used for digging dirt, gravel, sand etc.
* *sword* - Used for chopping leaves and grass. It is also an effective weapon.
* *hoe* - Used for farming.

These tools can be made of different materials, which change their efficiency. 
There are also other tools with specialist uses (e.g. screwdriver)

=== Protection
Protection blocks can be used to prevent strangers destroying the things you
build within a specific area. 

=== Locking
Locking requires a key, when you use shift-click a blank key against an item,
it will prevent them from using it.  This includes opening doors, opening
chests etc. 

=== Rail carts
Rail carts can be a way for a player to travel, they are faster than walking
for short distances, and for longer distances there is a special 'portal rail'
that can skip 500m in any direction. 

TIP: Vertical portal rails is the recommended way to travel back and forth from the
deepest mines.

=== Farming
Certain plants can be cultivated and grown. Find seeds by digging grass etc.
then use hoe to prepare grass or dirt near water for planting.  Plants will go
through several stages before being ready to harvest with a sword.

=== Mesecons
Later in the game when you have appropriate resources Mesecons can be used to
create electronic circuits. This is useful for building automatic farms, or
just fun contraptions.

=== Pipeworks
Vanilla uses a modified version of pipeworks that focuses on item tubes that
allows you to build pipelines for transporting sorting and distributing items. 

=== Flowers
Flowers grow in grasslands around the map, and are useful for decoration and
for making dye for coloring other blocks. Some flowers are much rarer than
others and they will spread over time. (*note this uses different mechanics
from legacy original minetest game)

=== Dying
When you die you will respawn at the last bed you slept in, or at that spawn
point.  All your items will be where you died in your 'bones' which you can go
back and collect if you can find them.

=== Special locations
There are scattered about the map special rare locations that usually hold
valuable treasure but can also be very dangerous. For example an Ice Tower can
only found at high up snowy locations and is guarded by deadly flying ice
monsters.

Protection blocks can only be found in treasure chests (e.g. in dungeons) they
cannot be crafted.
